/**
 * Application entry point
 */

import 'styles/index.scss';
import data from '../data/slider.json';
import Slider from '../app/slider.js';

window.addEventListener('load', () => {
  const slider = Slider(data);

  slider.init();
});



