'use strict'

import { getElement, toggleClass, removeClass, handleOnResize } from '../app/utils.js';

const Slider = ({ dataImages }) => {
  const SLIDE_DELAY = 7000;
  const TRANSITION_DELAY = 1500;
  const MIN_DESKTOP_SIZE = 812;
  const IMAGES_NUMBER = dataImages.length;
  const BACKGROUND_KEYS = ['bgImage', 'iconTrip', 'imageRocket', 'logo'];
  const TEXT_KEYS = ['easeView', 'hash', 'linknext', 'linkprev', 'subtitle', 'title'];
  
  // State
  let initTimeout = null;
  let sequenceInterval = null;
  let sequenceTimeout = null;
  let appElement = getElement('app');
  let loaderElm = getElement('loader');
  let currentIndex = 0;
  let currentData = dataImages[0];

  const isDesktop = () => window.innerWidth > MIN_DESKTOP_SIZE;

  const updateCurrentData = () => {
    if (currentIndex === (IMAGES_NUMBER - 1)) {
      currentIndex = 0;
      currentData = dataImages[0]
    } else {
      currentIndex=+1;
      currentData = dataImages[currentIndex]
    };
  };

  const filtered = allowed => Object.keys(currentData)
    .filter(key => allowed.includes(key))
    .reduce((obj, key) => {
      obj[key] = currentData[key];
      return obj;
    }, {});

  const dataBackground = () => filtered(BACKGROUND_KEYS);

  const dataText = () => filtered(TEXT_KEYS);

  const renderBackgrounds = () => {
    const items = dataBackground();

    Object.keys(items).map(key => {
      const domElements = document.querySelectorAll(`[data-bg='${key}']`);
      
      if(!domElements || (domElements.length === 0)) return false;
      
      domElements.forEach(element => {
        element.style.backgroundImage = `url(${items[key]})`;     
      });
    })
  };

  const renderTexts = () => {
    const items = dataText();

    Object.keys(items).map(key => {
      const domElement = getElement(key);
      if (domElement) domElement.innerHTML = items[key];
    })
  }

  const initSequence = () => {
    sequenceInterval = setInterval(() => {
      toggleStyles();
      sequenceTimeout = setTimeout(() => {
        updateCurrentData();
        renderBackgrounds();
        renderTexts();
        toggleStyles();
      }, TRANSITION_DELAY);
    }, SLIDE_DELAY);
  }

  const resetClasses = () => {
    removeClass(appElement, 'with-transition');
    removeClass(appElement, 'show');
    removeClass(loaderElm, 'with-transition');
    removeClass(loaderElm, 'full');
  }

  const resetStyles = () => {
    if (initTimeout) clearInterval(initTimeout);
    if (sequenceInterval) clearInterval(sequenceInterval);
    if (sequenceTimeout) clearInterval(sequenceTimeout);

    resetClasses();

    renderBackgrounds();
    renderTexts();

    // init slider styles
    toggleClass(appElement, 'with-transition');
    toggleClass(appElement, 'show');
    
    initTimeout = setTimeout(() => {
      toggleClass(loaderElm, 'with-transition');
      toggleClass(loaderElm, 'full');

      // init slider sequence
      isDesktop() && initSequence();
    }, 100);
  }

  const toggleStyles = () => {
    toggleClass(appElement, 'show');
    toggleClass(loaderElm, 'with-transition');
    toggleClass(loaderElm, 'full');
  }

  const sliderControls = () => {
    var controls = document.getElementsByClassName('slider__button');
    for (var i = 0; i < controls.length; i++) {
      var control = controls[i];
      control.onclick = e => {
        e.preventDefault();
        toggleClass(appElement, 'show');
        setTimeout(() => resetStyles(), 1000);
      }
    }
  }

  const init = () => {
    resetStyles();
    handleOnResize(() => resetStyles());
    sliderControls();
  }

  return {
    init
  }
}

export default Slider;