/**
 * JS Helpers
 */

import { debounce } from 'lodash';

export const getElement = id => document.getElementById(id);

export const toggleClass = (el, className) => {
  if (el.classList) {
    el.classList.toggle(className);
  } else {
    const classes = el.className.split(' ');
    const existingIndex = classes.indexOf(className);

    if (existingIndex >= 0)
      classes.splice(existingIndex, 1);
    else
      classes.push(className);

    el.className = classes.join(' ');
  }
};

export const removeClass = (el, className) => {
  if (el.classList)
    el.classList.remove(className);
  else
    el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
}

export const handleOnResize = callback => {
  window.addEventListener('resize', debounce(() => {
    callback();
  }, 500));
} 