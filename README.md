LeadTech Front-End Test
===========

## Configuración
Instalar dependencias
```sh
$ npm install
```

## Development
Para ejecutar el webpack-dev-server local y autocompilar en [http://localhost:8080/](http://localhost:8080/)
```sh
$ npm run dev
```

## Deployment
Para hacer un build de la aplicación
```sh
$ npm run build
```
